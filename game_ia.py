from game import SnakeGame
import tensorflow as tf
import pygame
from os import path
import numpy as np
import random

fileGameModel= "snakeia_model.h5"
fileGameData= "snakeia_data.npy"
fileGameValidation= "snakeia_validation.npy"

class SnakeNN:
    def __init__(self, game = SnakeGame(gui = False)):
        self.game = game
        self.size = int(game.board['width']/game.snake_block)

    def model(self):
        model = tf.keras.Sequential([
            tf.keras.layers.Flatten(input_shape=(6, 1)),
            tf.keras.layers.Dense(36, activation="relu"),
            tf.keras.layers.Dense(12, activation="relu"),
            tf.keras.layers.Dense(4, activation="softmax")
        ])

        model.compile( optimizer=tf.keras.optimizers.Adam(learning_rate=0.01),
                       loss=tf.keras.losses.MeanSquaredError())
        return model

    def gameDataToTrainData(self):
        data=[0, 0, 0, 0, 0, 0]
        #data colision
        data[SnakeGame.KEY_UP   ] = 1 if self.isNextColision(SnakeGame.KEY_UP) else  0
        data[SnakeGame.KEY_RIGHT] = 1 if self.isNextColision(SnakeGame.KEY_RIGHT) else  0
        data[SnakeGame.KEY_DOWN ] = 1 if self.isNextColision(SnakeGame.KEY_DOWN) else  0
        data[SnakeGame.KEY_LEFT ] = 1 if self.isNextColision(SnakeGame.KEY_LEFT) else  0
        data[4] = self.snakeFoodDeltaX()
        data[5] = self.snakeFoodDeltaY()
        return data

    def snakeFoodDeltaX(self):
        deltaX= self.game.snake[0][0] - self.game.food[0]
        return 0 if  deltaX==0 else 1 if deltaX > 0 else -1
    
    def snakeFoodDeltaY(self):
        deltaY= self.game.snake[0][1] - self.game.food[1]
        return 0 if  deltaY==0 else 1 if deltaY > 0 else -1

    def isGoodDirection(self, sugestion):
        deltaX = self.snakeFoodDeltaX()
        deltaY= self.snakeFoodDeltaY()
        if self.isNextColision(sugestion):
            return 0
        if deltaX > 0 and deltaY > 0 :
            return 1 if sugestion == SnakeGame.KEY_UP or sugestion == SnakeGame.KEY_LEFT else 0
        if deltaX > 0 and deltaY < 0 :
            return 1 if sugestion == SnakeGame.KEY_DOWN or sugestion == SnakeGame.KEY_LEFT else 0
        if deltaX > 0 and deltaY == 0 :
            return 1 if sugestion == SnakeGame.KEY_LEFT else 0
        
        if deltaX < 0 and deltaY > 0 :
            return 1 if sugestion == SnakeGame.KEY_UP or sugestion == SnakeGame.KEY_RIGHT else 0
        if deltaX < 0 and deltaY < 0 :
            return 1 if sugestion == SnakeGame.KEY_DOWN or sugestion == SnakeGame.KEY_RIGHT else 0
        if deltaX < 0 and deltaY == 0 :
            return 1 if sugestion == SnakeGame.KEY_RIGHT else 0
        
        if deltaX == 0 and deltaY > 0 :
            return 1 if sugestion == SnakeGame.KEY_UP else 0
        if deltaX == 0 and deltaY < 0 :
            return 1 if sugestion == SnakeGame.KEY_DOWN else 0
        return 1

    def isNextColision(self, key):
        new_point = self.game.new_point(key)
        return 1 if self.game.check_point_collisions(new_point) else 0

    def isBlocked(self, moves):
        for i in range(4):
            if moves[i]:
                return False
        return True    

    def trainData(self, gui=False, auto=False, nbrStep=1000):
        gameData= []
        gameValidation = []
        self.game.gui= gui
        while len(gameValidation) < nbrStep:
            currentMove= None
            currentStatus = None
            self.game.start()
            while not self.game.done:
                bestMoves= [1 if self.isGoodDirection(SnakeGame.KEY_UP) else 0,
                            1 if self.isGoodDirection(SnakeGame.KEY_RIGHT)else 0,
                            1 if self.isGoodDirection(SnakeGame.KEY_DOWN) else 0,
                            1 if self.isGoodDirection(SnakeGame.KEY_LEFT) else 0]
                moves= [0 if self.isNextColision(SnakeGame.KEY_UP) else 1,
                        0 if self.isNextColision(SnakeGame.KEY_RIGHT)else 1,
                        0 if self.isNextColision(SnakeGame.KEY_DOWN) else 1,
                        0 if self.isNextColision(SnakeGame.KEY_LEFT) else 1]
                if auto :
                    if currentStatus == None:
                        currentMove= random.randint(0,3)
                    else:
                        currentMove = np.argmax(bestMoves)
                else :
                    for event in pygame.event.get():
                        if event.type == pygame.KEYDOWN:
                            if event.key == pygame.K_LEFT:
                                currentMove= SnakeGame.KEY_LEFT
                            elif event.key == pygame.K_RIGHT:
                                currentMove= SnakeGame.KEY_RIGHT
                            elif event.key == pygame.K_UP:
                                currentMove= SnakeGame.KEY_UP
                            elif event.key == pygame.K_DOWN:
                                currentMove= SnakeGame.KEY_DOWN

                if currentMove != None:
                    trainData = self.gameDataToTrainData()
                    currentStatus= self.game.step(currentMove)
                    resultData= [0, 0, 0, 0]
                    if not self.game.done:
                        resultData[currentMove]=1
                    else:
                        moves[currentMove]=0
                        resultData=moves
                    
                    gameData.append(trainData)
                    gameValidation.append(resultData)
                
        #end of while training game
        gameData= np.array(gameData)
        gameValidation= np.array(gameValidation)
        if path.exists(fileGameData):
            dataFromFile= np.load(fileGameData)
            dataValidationFromFile= np.load(fileGameValidation)
            gameData= np.concatenate((dataFromFile, gameData))
            gameValidation= np.concatenate((dataValidationFromFile, gameValidation))
        
        print("score : ", self.game.score)
        print("validation data : ", len(gameValidation))
        model= self.model()
        if path.exists(fileGameModel):
            model.load_weights(fileGameModel)
        model.fit(tf.reshape(gameData, shape=[-1, 6]), gameValidation, epochs=100)
        model.save_weights(fileGameModel)
        np.save(fileGameData, gameData)
        np.save(fileGameValidation, gameValidation)

    def play(self, gui=False, nbrGame= 1000):
        model= self.model()
        if path.exists(fileGameModel):
            model.load_weights(fileGameModel)
        cpt_game=0
        self.game.gui= gui
        while cpt_game < nbrGame:
            gameData= []
            gameValidation = []
            blocked= False
            self.game.start()
            while not self.game.done:
                moves= [0 if self.isNextColision(SnakeGame.KEY_UP) else 1,
                        0 if self.isNextColision(SnakeGame.KEY_RIGHT)else 1,
                        0 if self.isNextColision(SnakeGame.KEY_DOWN) else 1,
                        0 if self.isNextColision(SnakeGame.KEY_LEFT) else 1]
                trainData = self.gameDataToTrainData()
                result = model.predict([trainData])
                prediction= np.argmax(result[0])
                resultData= result[0]
                blocked= self.isBlocked(moves)
                self.game.step(prediction)
                if self.game.done and not blocked:
                    moves[prediction]=0
                    resultData= moves
                    gameData.append(trainData)
                    gameValidation.append(resultData)

            cpt_game = cpt_game + 1

            #end of while training game
            gameData= np.array(gameData)
            gameValidation= np.array(gameValidation)
            if path.exists(fileGameData) and len(gameValidation)>0:
                dataFromFile= np.load(fileGameData)
                dataValidationFromFile= np.load(fileGameValidation)
                gameData= np.concatenate((dataFromFile, gameData))
                gameValidation= np.concatenate((dataValidationFromFile, gameValidation))
            
            print("score : ", self.game.score)
            print("validation data : ", len(gameValidation))
            if len(gameValidation)>0:
                model.fit(tf.reshape(gameData, shape=[-1, 6]), gameValidation, epochs=10)
                model.save_weights(fileGameModel)
                np.save(fileGameData, gameData)
                np.save(fileGameValidation, gameValidation)



if __name__ == "__main__":
    ia= SnakeNN()
    #ia.trainData(gui= True, auto=False, nbrStep=5)
    #ia.trainData(gui= False, auto=True, nbrStep=200)
    ia.play(gui= True, nbrGame=1000)
