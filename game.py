import pygame
import time
import random

white = (255, 255, 255)
yellow = (255, 255, 102)
black = (0, 0, 0)
red = (213, 50, 80)
green = (0, 255, 0)
blue = (50, 153, 213)


class SnakeGame:
    KEY_UP = 0
    KEY_RIGHT= 1     
    KEY_DOWN = 2      
    KEY_LEFT = 3  
    def __init__(self, board_width = 400, board_height = 400, gui = False):
        self.score = 0
        self.done = False
        self.snake_block = 10
        self.snake_speed = 20
        self.board = {'width': board_width, 'height': board_height}
        self.gui = gui

    def start(self):
        self.score = 0
        self.done = False
        self.snake_init()
        self.generate_food()
        if self.gui: self.render_init()
        return self.generate_observations()
    
    def snake_init(self):
        x = round(random.randint(0, self.board["width"] - self.snake_block) / 10.0) * 10.0
        y = round(random.randint(0, self.board["height"] - self.snake_block) / 10.0) * 10.0
        self.snake = []
        point = [x, y]
        self.snake.insert(0, point)

    def generate_food(self):
        food = []
        while food == []:
            food = [round(random.randint(0, self.board["width"] - self.snake_block) / 10.0) * 10.0, 
                    round(random.randint(0, self.board["height"] - self.snake_block) / 10.0) * 10.0]
            if food in self.snake: food = []
        self.food = food

    def render_init(self):
        pygame.init()
        dis = pygame.display.set_mode((self.board["width"], self.board["height"]))
        pygame.display.set_caption('Snake Game')
        self.clock = pygame.time.Clock()
        self.win= dis
        self.render()

    def render(self): 
        self.win.fill(black)
        pygame.draw.rect(self.win, red, [self.food[0], self.food[1], self.snake_block, self.snake_block])
        for x in self.snake:
            pygame.draw.rect(self.win, green, [x[0], x[1], self.snake_block, self.snake_block])
        score_font = pygame.font.SysFont("comicsansms", 35)
        value = score_font.render("Score: " + str(self.score), True, yellow)
        self.win.blit(value, [0, 0])
        pygame.display.update()
        self.clock.tick(self.snake_speed)

    def step(self, key):
        # 0 - UP
        # 1 - RIGHT
        # 2 - DOWN
        # 3 - LEFT
        if self.done == True: return self.generate_observations()#self.end_game()
        self.create_new_point(key)
        if self.food_eaten():
            self.score += 1
            #print("score:", self.score)
            self.generate_food()
        else:
            self.remove_last_point()
        self.check_collisions()
        if self.gui: self.render()
        return self.generate_observations()

    def create_new_point(self, key):
        new_point = self.new_point(key)
        self.snake.insert(0, new_point)

    def new_point(self, key):
        new_point = [self.snake[0][0], self.snake[0][1]]
        if key == self.KEY_UP:
            new_point[1] -= self.snake_block
        elif key == self.KEY_RIGHT:
            new_point[0] += self.snake_block
        elif key == self.KEY_DOWN:
            new_point[1] += self.snake_block
        elif key == self.KEY_LEFT:
            new_point[0] -= self.snake_block
        return new_point

    def remove_last_point(self):
        self.snake.pop()

    def food_eaten(self):
        return self.snake[0] == self.food

    def check_collisions(self):
        if (self.check_point_collisions(self.snake[0])):
            self.done = True
    
    def check_point_collisions(self, point):
        if (point[0] < 0 or
            point[0] > self.board["width"] - self.snake_block or
            point[1] < 0 or
            point[1] > self.board["height"] - self.snake_block or
            point in self.snake[1:-1]):
            return True
        else:
            return False

    def generate_observations(self):
        return self.done, self.score, self.snake, self.food

    def render_destroy(self):
        pygame.quit()

    def end_game(self):
        if self.gui: self.render_destroy()
        raise Exception("Game over")


def controleSnake(game):
    lastmove= None
    while not game.done:
        if lastmove != None:
            game.step(lastmove)
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    lastmove= 3 
                elif event.key == pygame.K_RIGHT:
                    lastmove= 1
                elif event.key == pygame.K_UP:
                    lastmove= 0
                elif event.key == pygame.K_DOWN:
                    lastmove= 2
                


if __name__ == "__main__":
    game = SnakeGame(gui = True)
    game.start()
    controleSnake(game)

